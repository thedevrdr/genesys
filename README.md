# Genesys

Just a simple text format for declaring basic components and convert them to rust data types under build-time.

No more repetitive macro calls or extensive declarations for simple structures, just a blissfull list of components with simple control of use of crate/module/function and derive for all, modules or specific structs.

Empowered by pest


**Todo**

- [x] Use crates/mods in modules
- [x] Custom derive for modules
- [ ] Impl Default for tuple structs
- [ ] Generate enums
- [ ] Identify single tuple structs primitive type to generate relevant ops code


## Usage

**build.rs**
```rust
use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=src/components");
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    genesys::generate("src/components", out_path.join("components.rs")).unwrap();
}
```

**main.rs/lib.rs**

Add this to the beginning of your file
```rust
include!(concat!(env!("OUT_DIR"), "/components.rs"));

```


## Cheat sheet, genesys format


```
# Default use, applied to all modules
global_use {
    # Comma-separated modules/functions
    bevy::prelude::Entity,
    bevy::prelude::*
}

# Default derive, applied to all components
global_derive (Debug, Entity)

# Components are either empty structs, tuple structs or named structs (enum comming soon)
# One component declaration per line

MyComponent
MyTupleComponent str # str gets translated to String
MyNamedComponent name: String, weight: f32

# Modules are declared with the modulus symbol "%"
%object {
    # Local use(overrides global use)
    use {
        entities::MySpecialEntity
    }

    # Local derive(overrides global)
    derive (MySpecialEntity)

    Name str
    Weight f32

    %item {
        Item
        Food
        TradeGood
        Weapon
        Armor
    }
}
```

## Binary

```
cargo run -- example/components # Prints components as rust
cargo run -- example/components components.rs # Writes output to components.rs
```


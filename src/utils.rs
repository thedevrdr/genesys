pub fn split(s: &str, delimiter: char) -> Vec<String> {
    let mut result = Vec::new();
    let mut is_quote = false;
    let mut last_index = 0;

    for (i, c) in s.char_indices() {
        match c {
            _ if c == delimiter => {
                if !is_quote {
                    result.push(String::from(&s[last_index..i]).trim().into());
                    last_index = i + 1;
                }
            }
            '"' => is_quote = is_quote == false,
            _ => {}
        }

        if i == s.len() - 1 {
            let mut last = &s[last_index..s.len()];
            result.push(String::from(last).trim().into());
        }
    }

    result
}

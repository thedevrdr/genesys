use std::io;
use std::path::PathBuf;

fn main() -> io::Result<()> {
    let args = std::env::args().collect::<Vec<String>>();

    if args.len() == 2 {
        if let Some(f) = args.get(1) {
            genesys::print(f)?;
        } else {
            println!("error: no args")
        }
    } else if args.len() == 3 {
        let (f1, f2) = (&args[1], &args[2]);
        genesys::generate(f1.as_str(), PathBuf::from(f2))?;
    } else {
        eprintln!("Invalid number of arguments");
        println!("genesys <component file.gen> [write_to_file.rs]");
    }

    Ok(())
}
